package com.reflection.app;

import com.reflection.domain.Person;
import com.reflection.domain.ValidationError;
import com.reflection.services.AnnotatedValidatorServices;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author aladin
 * @since 2/6/20
 */
public class Main {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        int age = sc.nextInt();
        Person p = new Person(name, age);
        List<ValidationError> errors = new ArrayList<>();
        AnnotatedValidatorServices.validate(p, errors);
        AnnotatedValidatorServices.print(errors);
    }
}
